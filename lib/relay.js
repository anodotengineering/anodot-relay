var net = require('net');
var Log = require('log');
var request = require('request');
var jpickle = require('jpickle');
var fs = require('fs');
var os = require('os');
var AnodotRelay = function () {

    var config = {};
    var metricsToSend = [];
    var timer = {};
    var log = {};
    var totalMetricsSent = 0, lastMetricsSent = 0;
    var metricsPerSecond = 0;
    var apiErrors = 0;
    var pendingRequests= 0, droppedSamples=0;
    var HEADER_LENGTH = 4, MAX_MESSAGE_LEN = 1000*1024;

    var HTTPOptions = {
        method: 'POST',
        body: null,
        json: true
    };

    function _send(body) {

        HTTPOptions.uri = config.connectionString;
        pendingRequests++;
        if(pendingRequests > config.pendingQueue){
            log.error('throwing samples due to large backlog. pending: ' + pendingRequests + ' dropped: ' + droppedSamples);
            pendingRequests--;
            droppedSamples++;
            return;
        }
        if  ( config.sortBeforeSend ){
            body.sort(function(a, b){ return a.timestamp - b.timestamp });
        }
        totalMetricsSent += body.length;
        HTTPOptions.body = body;
        log.debug('about to send: ' + JSON.stringify(HTTPOptions.body));
        request(HTTPOptions, function (error, response, body) {
            if (error) {
                log.error('failed to send to anodot ' + HTTPOptions.uri + ' ' + error.toString());
                apiErrors++;
            } else if (response.statusCode != 200) {
                apiErrors++;
                log.error('failed to send to anodot ' + HTTPOptions.uri + ' ' + 'status: ' + response.statusCode);
                log.info(body);
                response.resume();
            } else {
                response.resume();
            }
            pendingRequests--;
        });
    }
    function isCounter(metric) {
        var metricType;

        if (config.metricCounterRegex !== undefined){
            if ( metric.search(config.metricCounterRegex) !== -1){
                metricType = 'COUNTER';
            }else{
                metricType = 'GAUGE';
            }
        } else {
            metricType = 'GAUGE';
        }
        return metricType;
    }
    function _metricRewrite(metricName) {
        var j, match = [];
        if (config.metricRewriteRegex.length !== 0) {
            for (var j = 0; j < config.metricRewriteRegex.length; ++j) {
                // rewrite the first occurrence
                match = metricName.match(config.metricRewriteRegex[j].regex);
                if (match !== null) {
                    metricName = metricName.replace(match[0], config.metricRewriteRegex[j].token + match[0]);
                }
            }
        }
        return metricName;
    }
    var server = net.createServer(function (c) {
        var partial_data='';

        log.debug('server connected');
        c.setEncoding('ascii');

        c.on('end', function () {
            log.debug('server disconnected');
        });

        c.on('data', function (data) {
            var tmp=[], i, metric=[], isPartial=false, match = [], j, metricType = 'GAUGE';;

            log.debug('data: ' + data);

            //merge partial data with new data
            if(partial_data.length !== 0){
                data = partial_data.concat(data);
                partial_data = '';
            }

            //check if last line is complete
            if (data.charAt(data.length-1) !== '\n'){
                // trailer of the data is not complete
                isPartial = true;
            }
            // build json from data
            tmp = data.split('\n');
            for (i = 0; i < tmp.length; ++i) {
                if (tmp[i].length === 0) continue; // ignore empty strings
                // stop if data is partial
                if ((i === (tmp.length-1) && (isPartial))){
                    partial_data = tmp[i];
                    isPartial = false;
                    log.debug('got partial data: ' + tmp[i]);
                    break;
                }
                metric = tmp[i].split(' ');
                if ((metric[0] === undefined) ||
                    (metric[1] === undefined) ||
                    (metric[2] === undefined)){
                    log.warning('received illegal message entities ' + metric);
                    continue;
                }
                // run metric filter
                if (config.metricBlacklistRegex !== undefined){
                    if ( metric[0].search(config.metricBlacklistRegex) !== -1)
                        continue;
                }
                // run metric whitelist
                if (config.metricWhitelistRegex !== undefined){
                    if ( metric[0].search(config.metricWhitelistRegex) === -1)
                        continue;
                }
                // run metric rewrite
                metric[0] = _metricRewrite(metric[0]);

                // add prefix
                if ((config.metricPrefix !== undefined) && (config.metricPrefix !== '')) {
                    metric[0] = config.metricPrefix + '.' + metric[0];
                }

                metricType = isCounter(metric[0]);
                if ( metricType === 'COUNTER'){
                    metricsToSend.push({name: metric[0], timestamp: Number(metric[2]), value: Number(metric[1]),
                        tags: {target_type: metricType} });
                }else{
                    log.debug('pushing');
                    metricsToSend.push({name: metric[0], timestamp: Number(metric[2]), value: Number(metric[1])});
                }
            }
            log.debug(metricsToSend);

            if (metricsToSend.length >= 1000) {
                _send(metricsToSend);
                metricsToSend.length = 0;
            }
        });
    });

    var pickleServer = net.createServer(function (c) { //'connection' listener
        var partial_data = '';
        var position = 0;

        c.setEncoding('binary');

        c.on('end', function () {
            log.debug('server disconnected');
        });

        c.on('data', function (data) {

            // read graphite message header for length
            var buffer = new Buffer(HEADER_LENGTH);
            var message_length = 0, message = '', match = [], i,j;
            var metricType = 'GAUGE';

            if(partial_data.length !== 0){  // merge partial data with new data
                log.debug('concat partial data length: ' + data.length.toString(16) + ' with buffer length ' + partial_data.length.toString(16));
                data = partial_data.concat(data);
                partial_data = '';
            }
            while (data.length !== 0) {

                if(data.length < HEADER_LENGTH){ // partial header continue and read more data
                    log.debug('got partial header ' + data.length.toString(16));
                    partial_data = data;
                    break;
                }
                // read message header
                buffer.write(data, 0, HEADER_LENGTH, 'binary');
                message_length = buffer.readUInt32BE(0);

                if(message_length > MAX_MESSAGE_LEN){
                    log.error('message length too big: ', message_length.toString(16));
                    break;
                }
                // got partial body, continue and read more data from socket
                if (message_length > (data.length - HEADER_LENGTH)){
                    log.debug('got partial data ' + data.length.toString(16) + ' ' + message_length.toString(16));
                    partial_data = data;
                    break;
                }
                // calculate position used only for debugging
                position += (message_length + HEADER_LENGTH);
                log.info('position = ' + position.toString(16));
                log.info('message length ' + message_length.toString(16));
                log.info('--------------------');

                // slice one pickle message from input data and parse it
                message = data.slice(HEADER_LENGTH,  (HEADER_LENGTH + message_length));
                data = data.slice(HEADER_LENGTH + message_length);
                try {
                    var jdata = jpickle.loads(message);
                    if (jdata === undefined){
                        log.error('fail to parse pickle object');
                        break;
                    }
                    log.debug(jdata);
                    // save pickle message as json to be sent to anodot
                    for (i = 0; i < jdata.length; ++i) {

                        // run metric filter
                        if (config.metricBlacklistRegex !== undefined){
                            if ( jdata[i][0].search(config.metricBlacklistRegex) !== -1)
                                continue;
                        }
                        // run metric whitelist
                        if (config.metricWhitelistRegex !== undefined){
                            if ( jdata[i][0].search(config.metricWhitelistRegex) === -1)
                                continue;
                        }

                        // run metric rewrite
                        jdata[i][0] = _metricRewrite(jdata[i][0]);

                        // add prefix
                        if ((config.metricPrefix !== undefined) && (config.metricPrefix !== '')) {
                            jdata[i][0] = config.metricPrefix + '.' + jdata[i][0];
                        }

                        // check if metric is a counter
                        metricType = isCounter(jdata[i][0]);
                        if ( metricType === 'COUNTER'){
                            metricsToSend.push({name: jdata[i][0], timestamp: Number(jdata[i][1][0]), value: Number(jdata[i][1][1]),
                                tags: {target_type: metricType}});
                        }else{
                            metricsToSend.push({name: jdata[i][0], timestamp: Number(jdata[i][1][0]), value: Number(jdata[i][1][1]) });
                        }
                    }
                    log.debug(metricsToSend);

                } catch (error) {
                    log.error(error);
                }
            }
            if (metricsToSend.length >= 1000) {
                _send(metricsToSend);
                metricsToSend.length = 0;
            }
        });
    });

    function start (options) {

        if (options === undefined) options = {};
        config.url = options.url ||
            'http://api.anodot.com';
        config.token = options.token;
        config.api = options.api || '/api/v1/metrics?token=';
        config.connectionString = config.url  + config.api + config.token ;
        config.flushInterval = options.flushInterval || 10000;
        config.plainPort = options.plainPort || 2003;
        config.picklePort = options.picklePort || 2004;
        config.log = options.log || "info";
        config.showStats = options.showStats || false;
        config.metricRewriteRegex = [];
        config.pendingQueue = options.pendingQueue || 300;
        config.metricPrefix = options.metricPrefix || '';
        config.maxConnections = options.maxConnections || 100;
        config.backlog = options.backlog || 512;
        config.sortBeforeSend = options.sortBeforeSend || false;

        HTTPOptions.pool = {maxSockets: config.maxConnections };
        process.setMaxListeners(0); // work around to error from node js

        if (options.sendStats === undefined) {
            config.sendStats = true;
        }else{
            config.sendStats = options.sendStats;
        }

        if ((options.checkHTTPSCert !== undefined) && (options.checkHTTPSCert === false)){
            HTTPOptions.rejectUnauthorized = false;
        }

        if ( (options.metricFilter !== undefined) && (options.metricFilter !== "")){
            config.metricBlacklistRegex = new RegExp(options.metricFilter, "i");
        }

        if ( (options.metricBlacklist !== undefined) && (options.metricBlacklist !== "")){
            config.metricBlacklistRegex = new RegExp(options.metricBlacklist, "i");
        }

        if ( (options.metricWhitelist !== undefined) && (options.metricWhitelist !== "")){
            config.metricWhitelistRegex = new RegExp(options.metricWhitelist, "i");
        }

        if ( (options.metricCounter !== undefined) && (options.metricCounter !== "")){
            config.metricCounterRegex = new RegExp(options.metricCounter, "i");
        }


        if (options.metricRewrite !== undefined){
            for ( var i = 0; i < options.metricRewrite.length; ++i){
                if ((options.metricRewrite[i].regex === undefined) ||
                    (options.metricRewrite[i].token === undefined)){
                    log.error('illegal metric rewrite config: ' + config.metricRewriteRegex[i]);
                    continue;
                }
                config.metricRewriteRegex.push({'regex': new RegExp(options.metricRewrite[i].regex, "i"),
                                                'token': options.metricRewrite[i].token});
            }
        }

        log = new Log(config.log);

        log.debug(options);

        timer = setInterval(function() { // flush buffer to anodot every x seconds
            var currentTime = Math.round(new Date().getTime()/1000);
            var metricPrefix = 'anodot_host=' + os.hostname() + '.anodot_comp=relay.';

            metricsPerSecond = (totalMetricsSent - lastMetricsSent)/ (config.flushInterval/1000);
            lastMetricsSent = totalMetricsSent;
            if(config.sendStats){
                metricsToSend.push({'name': metricPrefix + 'what=metrics_per_second' , 'timestamp': currentTime, 'value': metricsPerSecond });
                metricsToSend.push({'name': metricPrefix + 'what=api_errors' , 'timestamp': currentTime, 'value': apiErrors });
                metricsToSend.push({'name': metricPrefix + 'what=dropped_samples' , 'timestamp': currentTime, 'value': droppedSamples });
                metricsToSend.push({'name': metricPrefix + 'what=uptime' , 'timestamp': currentTime, 'value': process.uptime() });
                metricsToSend.push({'name': metricPrefix + 'what=pending_requests' , 'timestamp': currentTime, 'value': pendingRequests });
            }


            if (metricsToSend.length !== 0) {
                log.debug('about to flush: ' + metricsToSend.length + ' metrics');
                _send(metricsToSend);
                metricsToSend.length = 0;
            }
            if (config.showStats){
                console.log('metricsPerSecond = ' + metricsPerSecond);
            }
        }, config.flushInterval);

        server.listen(config.plainPort, config.backlog, function () {
            log.info('start plain server');
        });
        pickleServer.listen(config.picklePort, config.backlog, function () {
            log.info('start pickle server');
        });
    }
    function stop () {
        server.close();
        pickleServer.close();
        clearInterval(timer);
    }
    return {
        start:start,
        stop:stop
    };
};
module.exports.relay = AnodotRelay();

